const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    password: {

        type: DataTypes.STRING,
        allowNull: false
    }
});

User.associate = function(models) {
    User.belongsToMany(models.Movie, {through: 'Rate', as: 'ratedMovies'});
    User.belongsToMany(models.Movie, {through: 'Favorite', as: 'favoritedMovies'});
    User.belongsToMany(models.User, {through: 'AddFriend', as: 'addedFriends'});
    User.belongsTo(models.Movie);
}

const User_Movie = sequelize.define('User_Profile', {
    rating: DataTypes.FLOAT
});

module.exports = User;