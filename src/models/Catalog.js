const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");
const Gender = require("./Gender");

const Catalog = sequelize.define('Catalog', {

    name: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

Catalog.associate = function(models) {
    Catalog.belongsToMany(models.Movie, {through: 'Has', as: 'movieTitle'});
    Catalog.belongsTo(models.Gender);
}

module.exports = Catalog;