const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Movie = sequelize.define('Movie', {

    title: {
        type: DataTypes.STRING,
        allowNull: false
    },

    average_rating: {
        type: DataTypes.FLOAT
    },

    sinopsis: {

        type: DataTypes.STRING,
        allowNull: false
    }
});

Movie.associate = function(models) {
    Movie.belongsToMany(models.User, {through: 'Rate', as: 'raterUsers'});
    Movie.belongsToMany(models.User, {through: 'Favorite', as: 'fanUsers'});
    Movie.belongsToMany(models.Catalog, {through: 'IsIn', as: 'pertencentCatalogs'});
    Movie.belongsToMany(models.Gender, {through: 'Has', as: 'movieGenders'});
    Movie.hasMany(models.User);
}

module.exports = Movie;