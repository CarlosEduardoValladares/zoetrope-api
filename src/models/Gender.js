const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");
const Catalog = require("./Catalog");

const Gender = sequelize.define('Gender', {

    name: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

Gender.associate = function(models) {
    Gender.belongsToMany(models.Movie, {through: 'IsIn', as: 'movieTitle'});
    Gender.hasMany(models.Catalog);
}

module.exports = Gender;