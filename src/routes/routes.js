const { Router } = require('express');
const UserController = require('../controllers/UserController');
const MovieController = require('../controllers/MovieController');
const GenderController = require('../controllers/GenderController');
const CatalogController = require('../controllers/CatalogController');
const router = Router();

router.get('/users',UserController.index);
router.get('/users/:id',UserController.show);
router.post('/users',UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);
router.put('/favorite/:id', UserController.favorite);
router.put('/unfavorite/:id', UserController.unfavorite);

router.get('/movies',MovieController.index);
router.get('/movies/:id',MovieController.show);
router.post('/movies',MovieController.create);
router.put('/movies/:id', MovieController.update);
router.delete('/movies/:id', MovieController.destroy);

router.get('/genders',GenderController.index);
router.get('/genders/:id',GenderController.show);
router.post('/genders',GenderController.create);
router.put('/genders/:id', GenderController.update);
router.delete('/genders/:id', GenderController.destroy);

router.get('/catalogs',CatalogController.index);
router.get('/catalogs/:id',CatalogController.show);
router.post('/catalogs',CatalogController.create);
router.put('/catalogs/:id', CatalogController.update);
router.delete('/catalogs/:id', CatalogController.destroy);

module.exports = router;