const { response } = require('express');
const Gender = require('../models/Gender');

const create = async(req,res) => {
    try{
          const gender = await Gender.create(req.body);
          return res.status(201).json({message: "Gênero cadastrado com sucesso!", gender: gender});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const index = async(req,res) => {
    try {
        const genders = await Gender.findAll();
        return res.status(200).json({genders});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const gender = await Gender.findByPk(id);
        return res.status(200).json({gender});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Gender.update(req.body, {where: {id: id}});
        if(updated) {
            const gender = await Gender.findByPk(id);
            return res.status(200).send(gender);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Gênero não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Gender.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Gênero deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Gênero não encontrado.");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
};