const { response } = require('express');
const Catalog = require('../models/Catalog');

const create = async(req,res) => {
    try{
          const catalog = await Catalog.create(req.body);
          return res.status(201).json({message: "Catálogo cadastrado com sucesso!", catalog: catalog});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const index = async(req,res) => {
    try {
        const catalogs = await Catalog.findAll();
        return res.status(200).json({catalogs});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const catalog = await Catalog.findByPk(id);
        return res.status(200).json({catalog});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Catalog.update(req.body, {where: {id: id}});
        if(updated) {
            const catalog = await Catalog.findByPk(id);
            return res.status(200).send(catalog);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Catálogo não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Catalog.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Catálogo deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Catálogo não encontrado.");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
};